package org.hipster.server.web.rest;

import org.hipster.server.ServerApp;
import org.hipster.server.domain.BwdHistory;
import org.hipster.server.repository.BwdHistoryRepository;
import org.hipster.server.service.BwdHistoryService;
import org.hipster.server.service.dto.BwdHistoryDTO;
import org.hipster.server.service.mapper.BwdHistoryMapper;
import org.hipster.server.service.dto.BwdHistoryCriteria;
import org.hipster.server.service.BwdHistoryQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BwdHistoryResource} REST controller.
 */
@SpringBootTest(classes = ServerApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class BwdHistoryResourceIT {

    private static final String DEFAULT_USER_ID = "AAAAAAAAAA";
    private static final String UPDATED_USER_ID = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_BWD_HASH = "AAAAAAAAAA";
    private static final String UPDATED_BWD_HASH = "BBBBBBBBBB";

    private static final String DEFAULT_BWD_CODE = "AAAAAAAAAA";
    private static final String UPDATED_BWD_CODE = "BBBBBBBBBB";

    @Autowired
    private BwdHistoryRepository bwdHistoryRepository;

    @Autowired
    private BwdHistoryMapper bwdHistoryMapper;

    @Autowired
    private BwdHistoryService bwdHistoryService;

    @Autowired
    private BwdHistoryQueryService bwdHistoryQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBwdHistoryMockMvc;

    private BwdHistory bwdHistory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BwdHistory createEntity(EntityManager em) {
        BwdHistory bwdHistory = new BwdHistory()
            .userId(DEFAULT_USER_ID)
            .createTime(DEFAULT_CREATE_TIME)
            .bwdHash(DEFAULT_BWD_HASH)
            .bwdCode(DEFAULT_BWD_CODE);
        return bwdHistory;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BwdHistory createUpdatedEntity(EntityManager em) {
        BwdHistory bwdHistory = new BwdHistory()
            .userId(UPDATED_USER_ID)
            .createTime(UPDATED_CREATE_TIME)
            .bwdHash(UPDATED_BWD_HASH)
            .bwdCode(UPDATED_BWD_CODE);
        return bwdHistory;
    }

    @BeforeEach
    public void initTest() {
        bwdHistory = createEntity(em);
    }

    @Test
    @Transactional
    public void createBwdHistory() throws Exception {
        int databaseSizeBeforeCreate = bwdHistoryRepository.findAll().size();
        // Create the BwdHistory
        BwdHistoryDTO bwdHistoryDTO = bwdHistoryMapper.toDto(bwdHistory);
        restBwdHistoryMockMvc.perform(post("/api/bwd-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bwdHistoryDTO)))
            .andExpect(status().isCreated());

        // Validate the BwdHistory in the database
        List<BwdHistory> bwdHistoryList = bwdHistoryRepository.findAll();
        assertThat(bwdHistoryList).hasSize(databaseSizeBeforeCreate + 1);
        BwdHistory testBwdHistory = bwdHistoryList.get(bwdHistoryList.size() - 1);
        assertThat(testBwdHistory.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testBwdHistory.getCreateTime()).isEqualTo(DEFAULT_CREATE_TIME);
        assertThat(testBwdHistory.getBwdHash()).isEqualTo(DEFAULT_BWD_HASH);
        assertThat(testBwdHistory.getBwdCode()).isEqualTo(DEFAULT_BWD_CODE);
    }

    @Test
    @Transactional
    public void createBwdHistoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = bwdHistoryRepository.findAll().size();

        // Create the BwdHistory with an existing ID
        bwdHistory.setId(1L);
        BwdHistoryDTO bwdHistoryDTO = bwdHistoryMapper.toDto(bwdHistory);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBwdHistoryMockMvc.perform(post("/api/bwd-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bwdHistoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BwdHistory in the database
        List<BwdHistory> bwdHistoryList = bwdHistoryRepository.findAll();
        assertThat(bwdHistoryList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkUserIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = bwdHistoryRepository.findAll().size();
        // set the field null
        bwdHistory.setUserId(null);

        // Create the BwdHistory, which fails.
        BwdHistoryDTO bwdHistoryDTO = bwdHistoryMapper.toDto(bwdHistory);


        restBwdHistoryMockMvc.perform(post("/api/bwd-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bwdHistoryDTO)))
            .andExpect(status().isBadRequest());

        List<BwdHistory> bwdHistoryList = bwdHistoryRepository.findAll();
        assertThat(bwdHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreateTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = bwdHistoryRepository.findAll().size();
        // set the field null
        bwdHistory.setCreateTime(null);

        // Create the BwdHistory, which fails.
        BwdHistoryDTO bwdHistoryDTO = bwdHistoryMapper.toDto(bwdHistory);


        restBwdHistoryMockMvc.perform(post("/api/bwd-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bwdHistoryDTO)))
            .andExpect(status().isBadRequest());

        List<BwdHistory> bwdHistoryList = bwdHistoryRepository.findAll();
        assertThat(bwdHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkBwdHashIsRequired() throws Exception {
        int databaseSizeBeforeTest = bwdHistoryRepository.findAll().size();
        // set the field null
        bwdHistory.setBwdHash(null);

        // Create the BwdHistory, which fails.
        BwdHistoryDTO bwdHistoryDTO = bwdHistoryMapper.toDto(bwdHistory);


        restBwdHistoryMockMvc.perform(post("/api/bwd-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bwdHistoryDTO)))
            .andExpect(status().isBadRequest());

        List<BwdHistory> bwdHistoryList = bwdHistoryRepository.findAll();
        assertThat(bwdHistoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBwdHistories() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList
        restBwdHistoryMockMvc.perform(get("/api/bwd-histories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bwdHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].createTime").value(hasItem(DEFAULT_CREATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].bwdHash").value(hasItem(DEFAULT_BWD_HASH)))
            .andExpect(jsonPath("$.[*].bwdCode").value(hasItem(DEFAULT_BWD_CODE)));
    }
    
    @Test
    @Transactional
    public void getBwdHistory() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get the bwdHistory
        restBwdHistoryMockMvc.perform(get("/api/bwd-histories/{id}", bwdHistory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(bwdHistory.getId().intValue()))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.createTime").value(DEFAULT_CREATE_TIME.toString()))
            .andExpect(jsonPath("$.bwdHash").value(DEFAULT_BWD_HASH))
            .andExpect(jsonPath("$.bwdCode").value(DEFAULT_BWD_CODE));
    }


    @Test
    @Transactional
    public void getBwdHistoriesByIdFiltering() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        Long id = bwdHistory.getId();

        defaultBwdHistoryShouldBeFound("id.equals=" + id);
        defaultBwdHistoryShouldNotBeFound("id.notEquals=" + id);

        defaultBwdHistoryShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultBwdHistoryShouldNotBeFound("id.greaterThan=" + id);

        defaultBwdHistoryShouldBeFound("id.lessThanOrEqual=" + id);
        defaultBwdHistoryShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllBwdHistoriesByUserIdIsEqualToSomething() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where userId equals to DEFAULT_USER_ID
        defaultBwdHistoryShouldBeFound("userId.equals=" + DEFAULT_USER_ID);

        // Get all the bwdHistoryList where userId equals to UPDATED_USER_ID
        defaultBwdHistoryShouldNotBeFound("userId.equals=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByUserIdIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where userId not equals to DEFAULT_USER_ID
        defaultBwdHistoryShouldNotBeFound("userId.notEquals=" + DEFAULT_USER_ID);

        // Get all the bwdHistoryList where userId not equals to UPDATED_USER_ID
        defaultBwdHistoryShouldBeFound("userId.notEquals=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByUserIdIsInShouldWork() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where userId in DEFAULT_USER_ID or UPDATED_USER_ID
        defaultBwdHistoryShouldBeFound("userId.in=" + DEFAULT_USER_ID + "," + UPDATED_USER_ID);

        // Get all the bwdHistoryList where userId equals to UPDATED_USER_ID
        defaultBwdHistoryShouldNotBeFound("userId.in=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByUserIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where userId is not null
        defaultBwdHistoryShouldBeFound("userId.specified=true");

        // Get all the bwdHistoryList where userId is null
        defaultBwdHistoryShouldNotBeFound("userId.specified=false");
    }
                @Test
    @Transactional
    public void getAllBwdHistoriesByUserIdContainsSomething() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where userId contains DEFAULT_USER_ID
        defaultBwdHistoryShouldBeFound("userId.contains=" + DEFAULT_USER_ID);

        // Get all the bwdHistoryList where userId contains UPDATED_USER_ID
        defaultBwdHistoryShouldNotBeFound("userId.contains=" + UPDATED_USER_ID);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByUserIdNotContainsSomething() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where userId does not contain DEFAULT_USER_ID
        defaultBwdHistoryShouldNotBeFound("userId.doesNotContain=" + DEFAULT_USER_ID);

        // Get all the bwdHistoryList where userId does not contain UPDATED_USER_ID
        defaultBwdHistoryShouldBeFound("userId.doesNotContain=" + UPDATED_USER_ID);
    }


    @Test
    @Transactional
    public void getAllBwdHistoriesByCreateTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where createTime equals to DEFAULT_CREATE_TIME
        defaultBwdHistoryShouldBeFound("createTime.equals=" + DEFAULT_CREATE_TIME);

        // Get all the bwdHistoryList where createTime equals to UPDATED_CREATE_TIME
        defaultBwdHistoryShouldNotBeFound("createTime.equals=" + UPDATED_CREATE_TIME);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByCreateTimeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where createTime not equals to DEFAULT_CREATE_TIME
        defaultBwdHistoryShouldNotBeFound("createTime.notEquals=" + DEFAULT_CREATE_TIME);

        // Get all the bwdHistoryList where createTime not equals to UPDATED_CREATE_TIME
        defaultBwdHistoryShouldBeFound("createTime.notEquals=" + UPDATED_CREATE_TIME);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByCreateTimeIsInShouldWork() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where createTime in DEFAULT_CREATE_TIME or UPDATED_CREATE_TIME
        defaultBwdHistoryShouldBeFound("createTime.in=" + DEFAULT_CREATE_TIME + "," + UPDATED_CREATE_TIME);

        // Get all the bwdHistoryList where createTime equals to UPDATED_CREATE_TIME
        defaultBwdHistoryShouldNotBeFound("createTime.in=" + UPDATED_CREATE_TIME);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByCreateTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where createTime is not null
        defaultBwdHistoryShouldBeFound("createTime.specified=true");

        // Get all the bwdHistoryList where createTime is null
        defaultBwdHistoryShouldNotBeFound("createTime.specified=false");
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByBwdHashIsEqualToSomething() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where bwdHash equals to DEFAULT_BWD_HASH
        defaultBwdHistoryShouldBeFound("bwdHash.equals=" + DEFAULT_BWD_HASH);

        // Get all the bwdHistoryList where bwdHash equals to UPDATED_BWD_HASH
        defaultBwdHistoryShouldNotBeFound("bwdHash.equals=" + UPDATED_BWD_HASH);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByBwdHashIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where bwdHash not equals to DEFAULT_BWD_HASH
        defaultBwdHistoryShouldNotBeFound("bwdHash.notEquals=" + DEFAULT_BWD_HASH);

        // Get all the bwdHistoryList where bwdHash not equals to UPDATED_BWD_HASH
        defaultBwdHistoryShouldBeFound("bwdHash.notEquals=" + UPDATED_BWD_HASH);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByBwdHashIsInShouldWork() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where bwdHash in DEFAULT_BWD_HASH or UPDATED_BWD_HASH
        defaultBwdHistoryShouldBeFound("bwdHash.in=" + DEFAULT_BWD_HASH + "," + UPDATED_BWD_HASH);

        // Get all the bwdHistoryList where bwdHash equals to UPDATED_BWD_HASH
        defaultBwdHistoryShouldNotBeFound("bwdHash.in=" + UPDATED_BWD_HASH);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByBwdHashIsNullOrNotNull() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where bwdHash is not null
        defaultBwdHistoryShouldBeFound("bwdHash.specified=true");

        // Get all the bwdHistoryList where bwdHash is null
        defaultBwdHistoryShouldNotBeFound("bwdHash.specified=false");
    }
                @Test
    @Transactional
    public void getAllBwdHistoriesByBwdHashContainsSomething() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where bwdHash contains DEFAULT_BWD_HASH
        defaultBwdHistoryShouldBeFound("bwdHash.contains=" + DEFAULT_BWD_HASH);

        // Get all the bwdHistoryList where bwdHash contains UPDATED_BWD_HASH
        defaultBwdHistoryShouldNotBeFound("bwdHash.contains=" + UPDATED_BWD_HASH);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByBwdHashNotContainsSomething() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where bwdHash does not contain DEFAULT_BWD_HASH
        defaultBwdHistoryShouldNotBeFound("bwdHash.doesNotContain=" + DEFAULT_BWD_HASH);

        // Get all the bwdHistoryList where bwdHash does not contain UPDATED_BWD_HASH
        defaultBwdHistoryShouldBeFound("bwdHash.doesNotContain=" + UPDATED_BWD_HASH);
    }


    @Test
    @Transactional
    public void getAllBwdHistoriesByBwdCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where bwdCode equals to DEFAULT_BWD_CODE
        defaultBwdHistoryShouldBeFound("bwdCode.equals=" + DEFAULT_BWD_CODE);

        // Get all the bwdHistoryList where bwdCode equals to UPDATED_BWD_CODE
        defaultBwdHistoryShouldNotBeFound("bwdCode.equals=" + UPDATED_BWD_CODE);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByBwdCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where bwdCode not equals to DEFAULT_BWD_CODE
        defaultBwdHistoryShouldNotBeFound("bwdCode.notEquals=" + DEFAULT_BWD_CODE);

        // Get all the bwdHistoryList where bwdCode not equals to UPDATED_BWD_CODE
        defaultBwdHistoryShouldBeFound("bwdCode.notEquals=" + UPDATED_BWD_CODE);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByBwdCodeIsInShouldWork() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where bwdCode in DEFAULT_BWD_CODE or UPDATED_BWD_CODE
        defaultBwdHistoryShouldBeFound("bwdCode.in=" + DEFAULT_BWD_CODE + "," + UPDATED_BWD_CODE);

        // Get all the bwdHistoryList where bwdCode equals to UPDATED_BWD_CODE
        defaultBwdHistoryShouldNotBeFound("bwdCode.in=" + UPDATED_BWD_CODE);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByBwdCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where bwdCode is not null
        defaultBwdHistoryShouldBeFound("bwdCode.specified=true");

        // Get all the bwdHistoryList where bwdCode is null
        defaultBwdHistoryShouldNotBeFound("bwdCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllBwdHistoriesByBwdCodeContainsSomething() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where bwdCode contains DEFAULT_BWD_CODE
        defaultBwdHistoryShouldBeFound("bwdCode.contains=" + DEFAULT_BWD_CODE);

        // Get all the bwdHistoryList where bwdCode contains UPDATED_BWD_CODE
        defaultBwdHistoryShouldNotBeFound("bwdCode.contains=" + UPDATED_BWD_CODE);
    }

    @Test
    @Transactional
    public void getAllBwdHistoriesByBwdCodeNotContainsSomething() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        // Get all the bwdHistoryList where bwdCode does not contain DEFAULT_BWD_CODE
        defaultBwdHistoryShouldNotBeFound("bwdCode.doesNotContain=" + DEFAULT_BWD_CODE);

        // Get all the bwdHistoryList where bwdCode does not contain UPDATED_BWD_CODE
        defaultBwdHistoryShouldBeFound("bwdCode.doesNotContain=" + UPDATED_BWD_CODE);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBwdHistoryShouldBeFound(String filter) throws Exception {
        restBwdHistoryMockMvc.perform(get("/api/bwd-histories?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(bwdHistory.getId().intValue())))
            .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
            .andExpect(jsonPath("$.[*].createTime").value(hasItem(DEFAULT_CREATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].bwdHash").value(hasItem(DEFAULT_BWD_HASH)))
            .andExpect(jsonPath("$.[*].bwdCode").value(hasItem(DEFAULT_BWD_CODE)));

        // Check, that the count call also returns 1
        restBwdHistoryMockMvc.perform(get("/api/bwd-histories/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBwdHistoryShouldNotBeFound(String filter) throws Exception {
        restBwdHistoryMockMvc.perform(get("/api/bwd-histories?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBwdHistoryMockMvc.perform(get("/api/bwd-histories/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingBwdHistory() throws Exception {
        // Get the bwdHistory
        restBwdHistoryMockMvc.perform(get("/api/bwd-histories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBwdHistory() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        int databaseSizeBeforeUpdate = bwdHistoryRepository.findAll().size();

        // Update the bwdHistory
        BwdHistory updatedBwdHistory = bwdHistoryRepository.findById(bwdHistory.getId()).get();
        // Disconnect from session so that the updates on updatedBwdHistory are not directly saved in db
        em.detach(updatedBwdHistory);
        updatedBwdHistory
            .userId(UPDATED_USER_ID)
            .createTime(UPDATED_CREATE_TIME)
            .bwdHash(UPDATED_BWD_HASH)
            .bwdCode(UPDATED_BWD_CODE);
        BwdHistoryDTO bwdHistoryDTO = bwdHistoryMapper.toDto(updatedBwdHistory);

        restBwdHistoryMockMvc.perform(put("/api/bwd-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bwdHistoryDTO)))
            .andExpect(status().isOk());

        // Validate the BwdHistory in the database
        List<BwdHistory> bwdHistoryList = bwdHistoryRepository.findAll();
        assertThat(bwdHistoryList).hasSize(databaseSizeBeforeUpdate);
        BwdHistory testBwdHistory = bwdHistoryList.get(bwdHistoryList.size() - 1);
        assertThat(testBwdHistory.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testBwdHistory.getCreateTime()).isEqualTo(UPDATED_CREATE_TIME);
        assertThat(testBwdHistory.getBwdHash()).isEqualTo(UPDATED_BWD_HASH);
        assertThat(testBwdHistory.getBwdCode()).isEqualTo(UPDATED_BWD_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingBwdHistory() throws Exception {
        int databaseSizeBeforeUpdate = bwdHistoryRepository.findAll().size();

        // Create the BwdHistory
        BwdHistoryDTO bwdHistoryDTO = bwdHistoryMapper.toDto(bwdHistory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBwdHistoryMockMvc.perform(put("/api/bwd-histories")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(bwdHistoryDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BwdHistory in the database
        List<BwdHistory> bwdHistoryList = bwdHistoryRepository.findAll();
        assertThat(bwdHistoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBwdHistory() throws Exception {
        // Initialize the database
        bwdHistoryRepository.saveAndFlush(bwdHistory);

        int databaseSizeBeforeDelete = bwdHistoryRepository.findAll().size();

        // Delete the bwdHistory
        restBwdHistoryMockMvc.perform(delete("/api/bwd-histories/{id}", bwdHistory.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BwdHistory> bwdHistoryList = bwdHistoryRepository.findAll();
        assertThat(bwdHistoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
