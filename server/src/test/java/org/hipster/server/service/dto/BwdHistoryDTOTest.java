package org.hipster.server.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.hipster.server.web.rest.TestUtil;

public class BwdHistoryDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BwdHistoryDTO.class);
        BwdHistoryDTO bwdHistoryDTO1 = new BwdHistoryDTO();
        bwdHistoryDTO1.setId(1L);
        BwdHistoryDTO bwdHistoryDTO2 = new BwdHistoryDTO();
        assertThat(bwdHistoryDTO1).isNotEqualTo(bwdHistoryDTO2);
        bwdHistoryDTO2.setId(bwdHistoryDTO1.getId());
        assertThat(bwdHistoryDTO1).isEqualTo(bwdHistoryDTO2);
        bwdHistoryDTO2.setId(2L);
        assertThat(bwdHistoryDTO1).isNotEqualTo(bwdHistoryDTO2);
        bwdHistoryDTO1.setId(null);
        assertThat(bwdHistoryDTO1).isNotEqualTo(bwdHistoryDTO2);
    }
}
