package org.hipster.server.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BwdHistoryMapperTest {

    private BwdHistoryMapper bwdHistoryMapper;

    @BeforeEach
    public void setUp() {
        bwdHistoryMapper = new BwdHistoryMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(bwdHistoryMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(bwdHistoryMapper.fromId(null)).isNull();
    }
}
