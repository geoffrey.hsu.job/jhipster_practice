package org.hipster.server.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.hipster.server.web.rest.TestUtil;

public class BwdHistoryTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BwdHistory.class);
        BwdHistory bwdHistory1 = new BwdHistory();
        bwdHistory1.setId(1L);
        BwdHistory bwdHistory2 = new BwdHistory();
        bwdHistory2.setId(bwdHistory1.getId());
        assertThat(bwdHistory1).isEqualTo(bwdHistory2);
        bwdHistory2.setId(2L);
        assertThat(bwdHistory1).isNotEqualTo(bwdHistory2);
        bwdHistory1.setId(null);
        assertThat(bwdHistory1).isNotEqualTo(bwdHistory2);
    }
}
