package org.hipster.server.repository;

import org.hipster.server.domain.BwdHistory;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BwdHistory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BwdHistoryRepository extends JpaRepository<BwdHistory, Long>, JpaSpecificationExecutor<BwdHistory> {
}
