package org.hipster.server.web.rest.custom;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.hipster.server.service.BwdHistoryQueryService;
import org.hipster.server.service.BwdHistoryService;
import org.hipster.server.service.dto.BwdHistoryCriteria;
import org.hipster.server.service.dto.BwdHistoryDTO;
import org.hipster.server.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.hipster.server.domain.BwdHistory}.
 */
@RestController
@RequestMapping("/api")
public class TestDataResource {

    private final Logger log = LoggerFactory.getLogger(TestDataResource.class);

    private static final String ENTITY_NAME = "bwdHistory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BwdHistoryService bwdHistoryService;

    private final BwdHistoryQueryService bwdHistoryQueryService;

    public TestDataResource(BwdHistoryService bwdHistoryService, BwdHistoryQueryService bwdHistoryQueryService) {
        this.bwdHistoryService = bwdHistoryService;
        this.bwdHistoryQueryService = bwdHistoryQueryService;
    }

    @PostMapping("/geoffrey")
    public ResponseEntity<BwdHistoryDTO> createGeoffreyTestData(@Valid @RequestBody BwdHistoryDTO bwdHistoryDTO) throws URISyntaxException {
        log.debug("REST request to save BwdHistory : {}", bwdHistoryDTO);
        if (bwdHistoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new bwdHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        bwdHistoryDTO.setUserId(bwdHistoryDTO.getUserId()+"_test");
        bwdHistoryDTO.setBwdCode(bwdHistoryDTO.getBwdCode()+"_test");
        BwdHistoryDTO result = bwdHistoryService.save(bwdHistoryDTO);
        log.debug("REST request to save BwdHistory : {}", result);
        return ResponseEntity.created(new URI("/api/bwd-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

}
