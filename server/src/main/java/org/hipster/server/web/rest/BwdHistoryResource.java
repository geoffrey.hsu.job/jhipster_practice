package org.hipster.server.web.rest;

import org.hipster.server.service.BwdHistoryService;
import org.hipster.server.web.rest.errors.BadRequestAlertException;
import org.hipster.server.service.dto.BwdHistoryDTO;
import org.hipster.server.service.dto.BwdHistoryCriteria;
import org.hipster.server.service.BwdHistoryQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.hipster.server.domain.BwdHistory}.
 */
@RestController
@RequestMapping("/api")
public class BwdHistoryResource {

    private final Logger log = LoggerFactory.getLogger(BwdHistoryResource.class);

    private static final String ENTITY_NAME = "bwdHistory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BwdHistoryService bwdHistoryService;

    private final BwdHistoryQueryService bwdHistoryQueryService;

    public BwdHistoryResource(BwdHistoryService bwdHistoryService, BwdHistoryQueryService bwdHistoryQueryService) {
        this.bwdHistoryService = bwdHistoryService;
        this.bwdHistoryQueryService = bwdHistoryQueryService;
    }

    /**
     * {@code POST  /bwd-histories} : Create a new bwdHistory.
     *
     * @param bwdHistoryDTO the bwdHistoryDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new bwdHistoryDTO, or with status {@code 400 (Bad Request)} if the bwdHistory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/bwd-histories")
    public ResponseEntity<BwdHistoryDTO> createBwdHistory(@Valid @RequestBody BwdHistoryDTO bwdHistoryDTO) throws URISyntaxException {
        log.debug("REST request to save BwdHistory : {}", bwdHistoryDTO);
        if (bwdHistoryDTO.getId() != null) {
            throw new BadRequestAlertException("A new bwdHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BwdHistoryDTO result = bwdHistoryService.save(bwdHistoryDTO);
        return ResponseEntity.created(new URI("/api/bwd-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /bwd-histories} : Updates an existing bwdHistory.
     *
     * @param bwdHistoryDTO the bwdHistoryDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated bwdHistoryDTO,
     * or with status {@code 400 (Bad Request)} if the bwdHistoryDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the bwdHistoryDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/bwd-histories")
    public ResponseEntity<BwdHistoryDTO> updateBwdHistory(@Valid @RequestBody BwdHistoryDTO bwdHistoryDTO) throws URISyntaxException {
        log.debug("REST request to update BwdHistory : {}", bwdHistoryDTO);
        if (bwdHistoryDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BwdHistoryDTO result = bwdHistoryService.save(bwdHistoryDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, bwdHistoryDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /bwd-histories} : get all the bwdHistories.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of bwdHistories in body.
     */
    @GetMapping("/bwd-histories")
    public ResponseEntity<List<BwdHistoryDTO>> getAllBwdHistories(BwdHistoryCriteria criteria, Pageable pageable) {
        log.debug("REST request to get BwdHistories by criteria: {}", criteria);
        Page<BwdHistoryDTO> page = bwdHistoryQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /bwd-histories/count} : count all the bwdHistories.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/bwd-histories/count")
    public ResponseEntity<Long> countBwdHistories(BwdHistoryCriteria criteria) {
        log.debug("REST request to count BwdHistories by criteria: {}", criteria);
        return ResponseEntity.ok().body(bwdHistoryQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /bwd-histories/:id} : get the "id" bwdHistory.
     *
     * @param id the id of the bwdHistoryDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the bwdHistoryDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/bwd-histories/{id}")
    public ResponseEntity<BwdHistoryDTO> getBwdHistory(@PathVariable Long id) {
        log.debug("REST request to get BwdHistory : {}", id);
        Optional<BwdHistoryDTO> bwdHistoryDTO = bwdHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(bwdHistoryDTO);
    }

    /**
     * {@code DELETE  /bwd-histories/:id} : delete the "id" bwdHistory.
     *
     * @param id the id of the bwdHistoryDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/bwd-histories/{id}")
    public ResponseEntity<Void> deleteBwdHistory(@PathVariable Long id) {
        log.debug("REST request to delete BwdHistory : {}", id);

        bwdHistoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
