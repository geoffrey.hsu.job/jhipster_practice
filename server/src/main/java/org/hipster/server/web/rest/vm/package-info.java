/**
 * View Models used by Spring MVC REST controllers.
 */
package org.hipster.server.web.rest.vm;
