package org.hipster.server.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;

/**
 * A BwdHistory.
 */
@Entity
@Table(name = "bwd_history")
public class BwdHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 140)
    @Column(name = "user_id", length = 140, nullable = false)
    private String userId;

    @NotNull
    @Column(name = "create_time", nullable = false)
    private Instant createTime;

    @NotNull
    @Size(max = 255)
    @Column(name = "bwd_hash", length = 255, nullable = false)
    private String bwdHash;

    @Size(max = 500)
    @Column(name = "bwd_code", length = 500)
    private String bwdCode;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public BwdHistory userId(String userId) {
        this.userId = userId;
        return this;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Instant getCreateTime() {
        return createTime;
    }

    public BwdHistory createTime(Instant createTime) {
        this.createTime = createTime;
        return this;
    }

    public void setCreateTime(Instant createTime) {
        this.createTime = createTime;
    }

    public String getBwdHash() {
        return bwdHash;
    }

    public BwdHistory bwdHash(String bwdHash) {
        this.bwdHash = bwdHash;
        return this;
    }

    public void setBwdHash(String bwdHash) {
        this.bwdHash = bwdHash;
    }

    public String getBwdCode() {
        return bwdCode;
    }

    public BwdHistory bwdCode(String bwdCode) {
        this.bwdCode = bwdCode;
        return this;
    }

    public void setBwdCode(String bwdCode) {
        this.bwdCode = bwdCode;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BwdHistory)) {
            return false;
        }
        return id != null && id.equals(((BwdHistory) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BwdHistory{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", createTime='" + getCreateTime() + "'" +
            ", bwdHash='" + getBwdHash() + "'" +
            ", bwdCode='" + getBwdCode() + "'" +
            "}";
    }
}
