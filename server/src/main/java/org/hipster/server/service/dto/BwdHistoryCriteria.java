package org.hipster.server.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link org.hipster.server.domain.BwdHistory} entity. This class is used
 * in {@link org.hipster.server.web.rest.BwdHistoryResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /bwd-histories?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BwdHistoryCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter userId;

    private InstantFilter createTime;

    private StringFilter bwdHash;

    private StringFilter bwdCode;

    public BwdHistoryCriteria() {
    }

    public BwdHistoryCriteria(BwdHistoryCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.createTime = other.createTime == null ? null : other.createTime.copy();
        this.bwdHash = other.bwdHash == null ? null : other.bwdHash.copy();
        this.bwdCode = other.bwdCode == null ? null : other.bwdCode.copy();
    }

    @Override
    public BwdHistoryCriteria copy() {
        return new BwdHistoryCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getUserId() {
        return userId;
    }

    public void setUserId(StringFilter userId) {
        this.userId = userId;
    }

    public InstantFilter getCreateTime() {
        return createTime;
    }

    public void setCreateTime(InstantFilter createTime) {
        this.createTime = createTime;
    }

    public StringFilter getBwdHash() {
        return bwdHash;
    }

    public void setBwdHash(StringFilter bwdHash) {
        this.bwdHash = bwdHash;
    }

    public StringFilter getBwdCode() {
        return bwdCode;
    }

    public void setBwdCode(StringFilter bwdCode) {
        this.bwdCode = bwdCode;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BwdHistoryCriteria that = (BwdHistoryCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(createTime, that.createTime) &&
            Objects.equals(bwdHash, that.bwdHash) &&
            Objects.equals(bwdCode, that.bwdCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        userId,
        createTime,
        bwdHash,
        bwdCode
        );
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BwdHistoryCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (createTime != null ? "createTime=" + createTime + ", " : "") +
                (bwdHash != null ? "bwdHash=" + bwdHash + ", " : "") +
                (bwdCode != null ? "bwdCode=" + bwdCode + ", " : "") +
            "}";
    }

}
