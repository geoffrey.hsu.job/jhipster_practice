package org.hipster.server.service.dto;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link org.hipster.server.domain.BwdHistory} entity.
 */
public class BwdHistoryDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 140)
    private String userId;

    @NotNull
    private Instant createTime;

    @NotNull
    @Size(max = 255)
    private String bwdHash;

    @Size(max = 500)
    private String bwdCode;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Instant getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Instant createTime) {
        this.createTime = createTime;
    }

    public String getBwdHash() {
        return bwdHash;
    }

    public void setBwdHash(String bwdHash) {
        this.bwdHash = bwdHash;
    }

    public String getBwdCode() {
        return bwdCode;
    }

    public void setBwdCode(String bwdCode) {
        this.bwdCode = bwdCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BwdHistoryDTO)) {
            return false;
        }

        return id != null && id.equals(((BwdHistoryDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BwdHistoryDTO{" +
            "id=" + getId() +
            ", userId='" + getUserId() + "'" +
            ", createTime='" + getCreateTime() + "'" +
            ", bwdHash='" + getBwdHash() + "'" +
            ", bwdCode='" + getBwdCode() + "'" +
            "}";
    }
}
