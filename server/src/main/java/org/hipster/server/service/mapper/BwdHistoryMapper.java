package org.hipster.server.service.mapper;


import org.hipster.server.domain.*;
import org.hipster.server.service.dto.BwdHistoryDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BwdHistory} and its DTO {@link BwdHistoryDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BwdHistoryMapper extends EntityMapper<BwdHistoryDTO, BwdHistory> {



    default BwdHistory fromId(Long id) {
        if (id == null) {
            return null;
        }
        BwdHistory bwdHistory = new BwdHistory();
        bwdHistory.setId(id);
        return bwdHistory;
    }
}
