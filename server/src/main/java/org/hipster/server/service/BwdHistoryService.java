package org.hipster.server.service;

import org.hipster.server.domain.BwdHistory;
import org.hipster.server.repository.BwdHistoryRepository;
import org.hipster.server.service.dto.BwdHistoryDTO;
import org.hipster.server.service.mapper.BwdHistoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link BwdHistory}.
 */
@Service
@Transactional
public class BwdHistoryService {

    private final Logger log = LoggerFactory.getLogger(BwdHistoryService.class);

    private final BwdHistoryRepository bwdHistoryRepository;

    private final BwdHistoryMapper bwdHistoryMapper;

    public BwdHistoryService(BwdHistoryRepository bwdHistoryRepository, BwdHistoryMapper bwdHistoryMapper) {
        this.bwdHistoryRepository = bwdHistoryRepository;
        this.bwdHistoryMapper = bwdHistoryMapper;
    }

    /**
     * Save a bwdHistory.
     *
     * @param bwdHistoryDTO the entity to save.
     * @return the persisted entity.
     */
    public BwdHistoryDTO save(BwdHistoryDTO bwdHistoryDTO) {
        log.debug("Request to save BwdHistory : {}", bwdHistoryDTO);
        BwdHistory bwdHistory = bwdHistoryMapper.toEntity(bwdHistoryDTO);
        bwdHistory = bwdHistoryRepository.save(bwdHistory);
        return bwdHistoryMapper.toDto(bwdHistory);
    }

    /**
     * Get all the bwdHistories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<BwdHistoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BwdHistories");
        return bwdHistoryRepository.findAll(pageable)
            .map(bwdHistoryMapper::toDto);
    }


    /**
     * Get one bwdHistory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BwdHistoryDTO> findOne(Long id) {
        log.debug("Request to get BwdHistory : {}", id);
        return bwdHistoryRepository.findById(id)
            .map(bwdHistoryMapper::toDto);
    }

    /**
     * Delete the bwdHistory by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete BwdHistory : {}", id);

        bwdHistoryRepository.deleteById(id);
    }
}
