import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { ClentSharedModule } from 'app/shared/shared.module';
import { ClentCoreModule } from 'app/core/core.module';
import { ClentAppRoutingModule } from './app-routing.module';
import { ClentHomeModule } from './home/home.module';
import { ClentEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { TestDataModule } from './test-data/test-data.module';
import { TestdataModule2 } from './testdata/testdata.module';

@NgModule({
  imports: [
    BrowserModule,
    ClentSharedModule,
    ClentCoreModule,
    ClentHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    ClentEntityModule,
    TestDataModule,
    TestdataModule2,
    ClentAppRoutingModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent],
})
export class ClentAppModule {}
