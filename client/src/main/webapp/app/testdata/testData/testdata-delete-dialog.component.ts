import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBwdHistory } from 'app/shared/model/bwd-history.model';
import { TestdataService } from './testdata.service';

@Component({
  templateUrl: './testdata-delete-dialog.component.html',
})
export class BwdHistoryDeleteDialogComponent {
  bwdHistory?: IBwdHistory;

  constructor(
    protected bwdHistoryService: TestdataService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.bwdHistoryService.delete(id).subscribe(() => {
      this.eventManager.broadcast('bwdHistoryListModification');
      this.activeModal.close();
    });
  }
}
