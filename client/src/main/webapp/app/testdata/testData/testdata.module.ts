import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ClentSharedModule } from 'app/shared/shared.module';
import { BwdHistoryComponent } from './testdata.component';
import { BwdHistoryDetailComponent } from './testdata-detail.component';
import { BwdHistoryUpdateComponent } from './testdata-update.component';
import { BwdHistoryDeleteDialogComponent } from './testdata-delete-dialog.component';
import { testdataRoute } from './testdata.route';
import { BwdHistoryDetailNewComponent } from './testdata-detail-new.component';

@NgModule({
  imports: [ClentSharedModule, RouterModule.forChild(testdataRoute)],
  declarations: [BwdHistoryComponent, BwdHistoryDetailComponent, BwdHistoryUpdateComponent, BwdHistoryDeleteDialogComponent, BwdHistoryDetailNewComponent],
  entryComponents: [BwdHistoryDeleteDialogComponent],
})
export class ClentBwdHistoryModule {}
