import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption, SearchWithPagination } from 'app/shared/util/request-util';
import { IBwdHistory } from 'app/shared/model/bwd-history.model';

type EntityResponseType = HttpResponse<IBwdHistory>;
type EntityArrayResponseType = HttpResponse<IBwdHistory[]>;

@Injectable({ providedIn: 'root' })
export class TestdataService {
  public resourceUrl = SERVER_API_URL + 'api/bwd-histories';
  public resourceSearchUrl = SERVER_API_URL + 'api/_search/bwd-histories';
  public resourceUrl_Geoffrey = SERVER_API_URL + 'api/geoffrey';

  constructor(protected http: HttpClient) {}

  create(bwdHistory: IBwdHistory): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bwdHistory);
    return this.http
      .post<IBwdHistory>(this.resourceUrl_Geoffrey, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(bwdHistory: IBwdHistory): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(bwdHistory);
    return this.http
      .put<IBwdHistory>(this.resourceUrl_Geoffrey, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBwdHistory>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBwdHistory[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  search(req: SearchWithPagination): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBwdHistory[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  protected convertDateFromClient(bwdHistory: IBwdHistory): IBwdHistory {
    const copy: IBwdHistory = Object.assign({}, bwdHistory, {
      createTime: bwdHistory.createTime && bwdHistory.createTime.isValid() ? bwdHistory.createTime.toJSON() : undefined,
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createTime = res.body.createTime ? moment(res.body.createTime) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((bwdHistory: IBwdHistory) => {
        bwdHistory.createTime = bwdHistory.createTime ? moment(bwdHistory.createTime) : undefined;
      });
    }
    return res;
  }
}
