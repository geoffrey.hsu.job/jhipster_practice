import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBwdHistory } from 'app/shared/model/bwd-history.model';

@Component({
  selector: 'jhi-bwd-history-detail',
  templateUrl: './bwd-history-detail.component.html',
})
export class BwdHistoryDetailComponent implements OnInit {
  bwdHistory: IBwdHistory | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bwdHistory }) => (this.bwdHistory = bwdHistory));
  }

  previousState(): void {
    window.history.back();
  }
}
