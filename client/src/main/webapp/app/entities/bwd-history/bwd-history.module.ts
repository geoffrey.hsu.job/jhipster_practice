import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ClentSharedModule } from 'app/shared/shared.module';
import { BwdHistoryComponent } from './bwd-history.component';
import { BwdHistoryDetailComponent } from './bwd-history-detail.component';
import { BwdHistoryUpdateComponent } from './bwd-history-update.component';
import { BwdHistoryDeleteDialogComponent } from './bwd-history-delete-dialog.component';
import { bwdHistoryRoute } from './bwd-history.route';

@NgModule({
  imports: [ClentSharedModule, RouterModule.forChild(bwdHistoryRoute)],
  declarations: [BwdHistoryComponent, BwdHistoryDetailComponent, BwdHistoryUpdateComponent, BwdHistoryDeleteDialogComponent],
  entryComponents: [BwdHistoryDeleteDialogComponent],
})
export class ClentBwdHistoryModule {}
