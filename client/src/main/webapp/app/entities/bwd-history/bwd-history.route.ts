import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBwdHistory, BwdHistory } from 'app/shared/model/bwd-history.model';
import { BwdHistoryService } from './bwd-history.service';
import { BwdHistoryComponent } from './bwd-history.component';
import { BwdHistoryDetailComponent } from './bwd-history-detail.component';
import { BwdHistoryUpdateComponent } from './bwd-history-update.component';

@Injectable({ providedIn: 'root' })
export class BwdHistoryResolve implements Resolve<IBwdHistory> {
  constructor(private service: BwdHistoryService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBwdHistory> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((bwdHistory: HttpResponse<BwdHistory>) => {
          if (bwdHistory.body) {
            return of(bwdHistory.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BwdHistory());
  }
}

export const bwdHistoryRoute: Routes = [
  {
    path: '',
    component: BwdHistoryComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams,
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,asc',
      pageTitle: 'clentApp.bwdHistory.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: BwdHistoryDetailComponent,
    resolve: {
      bwdHistory: BwdHistoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'clentApp.bwdHistory.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: BwdHistoryUpdateComponent,
    resolve: {
      bwdHistory: BwdHistoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'clentApp.bwdHistory.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: BwdHistoryUpdateComponent,
    resolve: {
      bwdHistory: BwdHistoryResolve,
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'clentApp.bwdHistory.home.title',
    },
    canActivate: [UserRouteAccessService],
  },
];
