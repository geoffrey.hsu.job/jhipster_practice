import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'bwd-history',
        loadChildren: () => import('./bwd-history/bwd-history.module').then(m => m.ClentBwdHistoryModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class ClentEntityModule {}
