import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IBwdHistory, BwdHistory } from 'app/shared/model/bwd-history.model';
import { TestDataService } from './test-data.service';

@Component({
  selector: 'jhi-bwd-history-update',
  templateUrl: './bwd-history-update.component.html',
})
export class BwdHistoryUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    userId: [null, [Validators.required, Validators.maxLength(140)]],
    createTime: [null, [Validators.required]],
    bwdHash: [null, [Validators.required, Validators.maxLength(255)]],
    bwdCode: [null, [Validators.maxLength(500)]],
  });

  constructor(protected bwdHistoryService: TestDataService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ bwdHistory }) => {
      if (!bwdHistory.id) {
        const today = moment().startOf('day');
        bwdHistory.createTime = today;
      }

      this.updateForm(bwdHistory);
    });
  }

  updateForm(bwdHistory: IBwdHistory): void {
    this.editForm.patchValue({
      id: bwdHistory.id,
      userId: bwdHistory.userId,
      createTime: bwdHistory.createTime ? bwdHistory.createTime.format(DATE_TIME_FORMAT) : null,
      bwdHash: bwdHistory.bwdHash,
      bwdCode: bwdHistory.bwdCode,
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const bwdHistory = this.createFromForm();
    if (bwdHistory.id !== undefined) {
      this.subscribeToSaveResponse(this.bwdHistoryService.update(bwdHistory));
    } else {
      this.subscribeToSaveResponse(this.bwdHistoryService.create(bwdHistory));
    }
  }

  private createFromForm(): IBwdHistory {
    return {
      ...new BwdHistory(),
      id: this.editForm.get(['id'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      createTime: this.editForm.get(['createTime'])!.value ? moment(this.editForm.get(['createTime'])!.value, DATE_TIME_FORMAT) : undefined,
      bwdHash: this.editForm.get(['bwdHash'])!.value,
      bwdCode: this.editForm.get(['bwdCode'])!.value,
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBwdHistory>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
