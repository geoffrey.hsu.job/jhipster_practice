import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBwdHistory } from 'app/shared/model/bwd-history.model';
import { TestDataService } from './test-data.service';

@Component({
  templateUrl: './bwd-history-delete-dialog.component.html',
})
export class BwdHistoryDeleteDialogComponent {
  bwdHistory?: IBwdHistory;

  constructor(
    protected bwdHistoryService: TestDataService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.bwdHistoryService.delete(id).subscribe(() => {
      this.eventManager.broadcast('bwdHistoryListModification');
      this.activeModal.close();
    });
  }
}
