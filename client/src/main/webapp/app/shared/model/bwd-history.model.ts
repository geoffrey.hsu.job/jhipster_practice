import { Moment } from 'moment';

export interface IBwdHistory {
  id?: number;
  userId?: string;
  createTime?: Moment;
  bwdHash?: string;
  bwdCode?: string;
}

export class BwdHistory implements IBwdHistory {
  constructor(public id?: number, public userId?: string, public createTime?: Moment, public bwdHash?: string, public bwdCode?: string) {}
}
