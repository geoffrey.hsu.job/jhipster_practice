import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { ClentTestModule } from '../../../test.module';
import { BwdHistoryUpdateComponent } from 'app/entities/bwd-history/bwd-history-update.component';
import { BwdHistoryService } from 'app/entities/bwd-history/bwd-history.service';
import { BwdHistory } from 'app/shared/model/bwd-history.model';

describe('Component Tests', () => {
  describe('BwdHistory Management Update Component', () => {
    let comp: BwdHistoryUpdateComponent;
    let fixture: ComponentFixture<BwdHistoryUpdateComponent>;
    let service: BwdHistoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ClentTestModule],
        declarations: [BwdHistoryUpdateComponent],
        providers: [FormBuilder],
      })
        .overrideTemplate(BwdHistoryUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BwdHistoryUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BwdHistoryService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BwdHistory(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BwdHistory();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
