import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ClentTestModule } from '../../../test.module';
import { BwdHistoryDetailComponent } from 'app/entities/bwd-history/bwd-history-detail.component';
import { BwdHistory } from 'app/shared/model/bwd-history.model';

describe('Component Tests', () => {
  describe('BwdHistory Management Detail Component', () => {
    let comp: BwdHistoryDetailComponent;
    let fixture: ComponentFixture<BwdHistoryDetailComponent>;
    const route = ({ data: of({ bwdHistory: new BwdHistory(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [ClentTestModule],
        declarations: [BwdHistoryDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }],
      })
        .overrideTemplate(BwdHistoryDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BwdHistoryDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load bwdHistory on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.bwdHistory).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
